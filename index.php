<?php
	session_start(); 
	$link = mysqli_connect('localhost', 'root', 'root');
	mysqli_select_db($link, 'messenger');

	$userid = 5;

	$query = "select m.*, sender.name as sender_name, receiver.name as receiver_name from message as m left join users as sender on m.sender = sender.id left join users as receiver on m.receiver = receiver.id where (m.receiver = ".$userid." or m.sender = ".$userid.") order by m.time asc";

	$result = mysqli_query($link, $query);

	$query = "select count(id) as count from message where receiver = ".$userid." and status = 0";
	$count_result = mysqli_fetch_array(mysqli_query($link, $query));

	$unread_cnt = $count_result['count'];

	$query = "SELECT id, name FROM users where id <> ".$userid."";
	$user_result = mysqli_query($link, $query);
	$users = array();

	while($user = mysqli_fetch_array($user_result)) { $users[] = $user; }
?>

<!DOCTYPE html>
<head>
	<title>MESSENGER</title>
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/cover.css">
</head>
<body>
	<div class="site-wrapper">
		<div class="site-wrapper-inner">
			<div class="cover-container">
				<div class="message-container">
					<div class="row message-header">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6"><span class="msg-title">Messages</span>
								<span class="msg-cnt"></span>
								</div>
								<div class="col-md-6 pull-right text-right push-notification">
									Push notifications <span class="notify-status">OFF</span>
								</div>
							</div>
						</div>
					</div>
					<div class="row message-body">
					<?php
						while($row = mysqli_fetch_array($result)) {
							$query = "select * from reply where message_id = ".$row['id']."";
							$reply_result = mysqli_query($link, $query);
							$result_cnt = mysqli_num_rows($reply_result);
							$reply = "";
							$class = "";

							if($row['sender'] == $userid) {
								$class = "pull-right text-right";
							} elseif($row['status'] == 0) {
								$class = "unread";
							}

							if($result_cnt > 0) {
								$reply = mysqli_fetch_array($reply_result);
								if($reply['status'] == 0) {
									$class .= " unread";
									$unread_cnt++;
								}
							}
					?>
						<div class="col-md-12 message <?php echo $class; ?>" data-id="<?php echo $row['id']; ?>">
							<div class="row">
								<div class="message-item <?php echo ($row['sender'] != $userid) ? 'col-md-10 received' : 'col-md-12 sent'?>">
									<div class="msg-sender">
										<?php echo ($row['sender'] != $userid ) ? $row['sender_name'] : $row['receiver_name']; ?><br>
										<span class="msg-time"><?php echo date('d.m.Y - H:i a', strtotime($row['time'])); ?></span>
									</div>
									<div class="msg-content">
										<?php echo $row['content']; ?>
									</div>
								</div>
								<?php 
									if(!empty($reply)) {
									?>
									<div class="col-md-10 <?php echo ($row['sender'] != $userid) ? 'pull-right text-right' : 'pull-left text-left'; ?>">
										<div class="reply-sender">
											<?php echo $row['receiver_name'];?><br>
											<span class="reply-time"><?php echo $reply['time']; ?></span>
										</div>
										<div class="reply-content">
											<?php echo $reply['content']; ?>
										</div>
									</div>
									<?php
									} else {
									?>
									<?php if ($row['sender'] != $userid) { ?>
									<div class="col-md-2 buttons">
										<button class="btn btn-primary btn-reply"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span></button>
										<button class="btn btn-primary btn-forward"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></button>
									</div>
									<?php } ?>
									<?php
									}
								?>	
							</div>

							<?php if(empty($reply)) { ?>
							<div class="row reply-message-wrapper">
								<div class="col-md-10 pull-right text-right">
									<input class="form-control reply-message-input" type="text" placeholder="New Message">
									<button class="btn btn-primary btn-reply-send" data-msg-id="<?php echo $row['id']; ?>">Send</button>
									<button class="btn btn-primary btn-reply-cancel">Canel</button>
								</div>
							</div>
							<?php } ?>
							<div class="row forward-message-wrapper">
								<div class="col-md-10 pull-right text-right forward-contact-list-wrapper">
									<div class="forward-contact-list">
										<input type="checkbox" class="select-all" id="check_<?php echo $row['id'];?>_0" value="0" data-name="0" />&nbsp;&nbsp;<label for="check_<?php echo $row['id'];?>_0">Select All</label>
										<hr>
										<?php 
										foreach ($users as $user) { 
											if($user['id'] != $row['sender']) {
										?>

											<input type="checkbox" id="check_<?php echo $row['id'].'_'.$user['id'];?>" value="<?php echo $user['id']; ?>" data-name="<?php echo $user['name'];?>"/>&nbsp;&nbsp;<label for="check_<?php echo $row['id'].'_'.$user['id']; ?>"><?php echo $user['name'];?></label><br>
										<?php 
											}
										}	
										?>
									</div>
									<button class="btn btn-primary btn-forward-contact">Contacts</button>
									<button class="btn btn-primary btn-forward-send">Send</button>
									<button class="btn btn-primary btn-forward-cancel">Cancel</button>
								</div>
							</div>
						</div>
					<?php
						}
					?>
					</div>
					<div class="row message-send">
						<div class="col-md-10">
							<input class="form-control new-message-input" type="text" placeholder="New Message">
						</div>
						<div class="col-md-2 contact-list-wrapper">
							<div class="contact-list">
								<input type="checkbox" class="select-all" id="check_0" value="0" data-name="0" />&nbsp;&nbsp;<label for="check_0">Select all</label><hr>
								<?php foreach($users as $user) { ?>
									<input type="checkbox" id="check_<?php echo $user['id'];?>" value="<?php echo $user['id']; ?>" data-name="<?php echo $user['name'];?>"/>&nbsp;&nbsp;<label for="check_<?php echo $user['id']; ?>"><?php echo $user['name'];?></label><br>
								<?php }	?>
 							</div>
							<button class="btn btn-primary btn-contact"><span class="glyphicon glyphicon-book" aria-hidden="true"></span></button>
							<button class="btn btn-primary btn-send"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></button>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>

<script type="text/javascript" src="assets/js/jquery-3.2.0.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/jquery.nicescroll.min.js"></script>

<script type='text/javascript'>
$(document).ready(function(){
	var unread_cnt = <?php echo $unread_cnt; ?>;
	$('.msg-cnt').html(unread_cnt);
	if(unread_cnt > 0) $('.msg-cnt').css('visibility', 'visible');

	function loadMessage() {
		var received_cnt = $('.message-body .message-item.received').length;
		var obj = $('.message-body .message-item.received')[received_cnt-1];
		var last_time = $(obj).find('.msg-time').html();
		
		$.ajax({
			url: 'load_message.php',
			data: {user_id : <?php echo $userid; ?>, last_time : last_time},
			method: 'post',
			success: function(msg) {
				if(msg!='no records') {
					var objs = jQuery.parseJSON(msg);
					$('.msg-cnt').html(parseInt($('.msg-cnt').html())+objs.length);
					$('.msg-cnt').css('visibility', 'visible');
					for(i=0; i<objs.length; i++) {
						var object = objs[i];
						console.log(object);
						var content = '<div class="col-md-12 message unread" data-id="'+object.id+'"><div class="row"><div class="message-item col-md-10 received"><div class="msg-sender">'+object.sender_name+'<br><span class="msg-time">'+object.time+'</span></div><div class="msg-content">'+object.content+'</div></div><div class="col-md-2 buttons"><button class="btn btn-primary btn-reply"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span></button><button class="btn btn-primary btn-forward"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></button></div></div><div class="row reply-message-wrapper"><div class="col-md-10 pull-right text-right"><input class="form-control reply-message-input" type="text" placeholder="New Message"><button class="btn btn-primary btn-reply-send" data-msg-id="'+object.id+'">Send</button><button class="btn btn-primary btn-reply-cancel">Canel</button></div></div></div>';
						$('.message-body').append(content);
					}

					$('.message-body').niceScroll().resize();
					$('.message-body').scrollTop($('.message-body').prop("scrollHeight"));
				}
			}
		});
	}

	setInterval(loadMessage, 1000);

	$('body').on('click', '.message', function(){
		$('.msg-content, .reply-content').css({'overflow': 'hidden', 'white-space':'nowrap', 'text-overflow':'ellipsis'});

		$(this).find('.msg-content, .reply-content').css({'overflow': 'visible', 'white-space':'normal', 'text-overflow':'clip'});

		$('.message-body').niceScroll().resize();

		if(!$(this).hasClass('unread')) return;
		id = $(this).data('id');
		$message = $(this);
		$message_cnt = $('.message-header .msg-cnt');
		$.ajax({
			url: 'post_status_change.php',
			data: {id: id},
			method: 'post',
			success: function(msg){
				$message.removeClass('unread');
				$unread_cnt = parseInt($message_cnt.html());
				$unread_cnt -= 1;
				$message_cnt.html($unread_cnt);

				if($unread_cnt == 0) $message_cnt.css('display', 'none');
			}
		});
	});

	$('body').on('click', '.btn-reply-send', function(){
		$('.message-body').niceScroll().resize();

		$this = $(this);
		msg_id = $this.data('msg-id');
		msg = $this.parent().find('.reply-message-input').val();

		$.ajax({
			url: 'add_reply.php',
			data: {id: msg_id, reply_msg: msg},
			method: 'post',
			success: function(ret) {
				if(ret != 'fail') {
					content = '<div class="col-md-10 pull-right text-right"><div class="reply-sender">Alex Mason<br><span class="reply-time">'+ret+'</span></div><div class="reply-content">'+msg+'</div></div>';

					$target = $this.closest('.message').find('.message-item');
					$(content).insertAfter($target);
					$this.closest('.message').find('.buttons').remove();
					$this.closest('.reply-message-wrapper').remove();
					$('.message-body').getNiceScroll().resize();
				}
			}
		});
	});

	$('body').on('click', '.btn-reply', function(){
		$(this).closest('.message').find('.reply-message-wrapper').slideToggle();
	});

	$('body').on('click', '.btn-forward', function(){
		$(this).closest('.message').find('.forward-message-wrapper').slideToggle();
	});

	$('body').on('click', '.btn-reply-cancel', function(){
		$(this).closest('.reply-message-wrapper').slideToggle();
	});

	$('body').on('click', '.btn-contact', function(){
		$(this).parent().find('.contact-list').fadeToggle();
	});

	$('body').on('click', '.btn-forward-contact', function(){
		$(this).parent().find('.forward-contact-list').fadeToggle();
	});

	$('body').on('click', '.btn-forward-cancel', function(){
		$(this).parent().find('.forward-contact-list').fadeToggle();
	});

	$('body').on('click', '.select-all', function() {
		$(this).parent().find('input[type=checkbox]').prop('checked', $(this).prop('checked'));
	});

	$('body').on('click', '.push-notification', function(){
		$notify_status = $(this).find('.notify-status');
		if($notify_status.html() == 'ON') {
			$notify_status.html('OFF');
		} else {
			$notify_status.html('ON');
		}
	});

	$('body').on('click', '.btn-forward-send', function(){
		$(this).parent().find('.forward-contact-list').fadeOut();
		var msg = $(this).closest('.message').find('.msg-content').html();
		var contacts = [];
		var contacts_name = [];

		$(this).parent().find('.forward-contact-list input[type=checkbox]').each(function(){
			if($(this).prop('checked')) {
				contacts.push($(this).val());
				contacts_name.push($(this).data('name'));
			}
		});

		if(contacts.length == 0) { alert('Please select cotnact'); return }

		$.ajax({
			url: 'send_message.php',
			data: {msg: msg, receivers: JSON.stringify(contacts)},
			method: 'post',
			success: function(ret) {
				if(ret != 'fail') {
					for(var i=0 ;i<contacts_name.length; i++) {
						if(contacts_name[i] != '0') {
							content = '<div class="col-md-12 message pull-right text-right"><div class="row"><div class="col-md-12 message-item"><div class="msg-sender">'+contacts_name[i]+'<br><span class="msg-time">'+ret+'</span></div><div class="msg-content">'+msg+'</div></div></div></div>';
							$('.message-body').append(content);
						}
					}
					$('.message-body').getNiceScroll().resize();
					$('.message-body').scrollTop($('.message-body').prop("scrollHeight"));
				}
			}
		});
	});

	$('body').on('click', '.btn-send', function(){
		$(this).parent().find('.contact-list').fadeOut();
		var msg = $('.new-message-input').val();

		var contacts = [];
		var contacts_name = [];
		
		$('.contact-list input[type=checkbox]').each(function(){
			if($(this).prop('checked')) {
				contacts.push($(this).val());
				contacts_name.push($(this).data('name'));
			}
		});

		if(msg == '') { alert('Please iput message'); return }
		if(contacts.length == 0) { alert('Please select contact'); return }

		$.ajax({
			url: 'send_message.php',
			data: {msg: msg, receivers: JSON.stringify(contacts)},
			method: 'post',
			success: function(ret) {
				if(ret != 'fail') {
					for(var i=0 ;i<contacts_name.length; i++) {
						if(contacts_name[i] != '0') {
							content = '<div class="col-md-12 message pull-right text-right"><div class="row"><div class="col-md-12 message-item"><div class="msg-sender">'+contacts_name[i]+'<br><span class="msg-time">'+ret+'</span></div><div class="msg-content">'+msg+'</div></div></div></div>';
							$('.message-body').append(content);
						}
					}
					$('.message-body').getNiceScroll().resize();
					$('.message-body').scrollTop($('.message-body').prop("scrollHeight"));
				}
			}
		})
	});
	$('.message-body').niceScroll();
});
</script>

</body>
</html>
